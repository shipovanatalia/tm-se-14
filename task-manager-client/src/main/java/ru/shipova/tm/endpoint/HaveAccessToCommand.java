
package ru.shipova.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for haveAccessToCommand complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="haveAccessToCommand"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.tm.shipova.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="needAuthorize" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="isOnlyAdminCommand" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "haveAccessToCommand", propOrder = {
    "session",
    "needAuthorize",
    "isOnlyAdminCommand"
})
public class HaveAccessToCommand {

    protected Session session;
    protected boolean needAuthorize;
    protected boolean isOnlyAdminCommand;

    /**
     * Gets the value of the session property.
     * 
     * @return
     *     possible object is
     *     {@link Session }
     *     
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     * 
     * @param value
     *     allowed object is
     *     {@link Session }
     *     
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the needAuthorize property.
     * 
     */
    public boolean isNeedAuthorize() {
        return needAuthorize;
    }

    /**
     * Sets the value of the needAuthorize property.
     * 
     */
    public void setNeedAuthorize(boolean value) {
        this.needAuthorize = value;
    }

    /**
     * Gets the value of the isOnlyAdminCommand property.
     * 
     */
    public boolean isIsOnlyAdminCommand() {
        return isOnlyAdminCommand;
    }

    /**
     * Sets the value of the isOnlyAdminCommand property.
     * 
     */
    public void setIsOnlyAdminCommand(boolean value) {
        this.isOnlyAdminCommand = value;
    }

}
