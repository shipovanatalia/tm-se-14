package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();

            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[TASK LIST]");
                @Nullable final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
                if (taskEndpoint == null) return;
                if (sessionDTO == null) return;
                @Nullable final List<TaskDTO> taskList = taskEndpoint.getTaskListOfUser(sessionDTO);
                if (taskList == null) return;
                sortTasks(taskList, sessionDTO);
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    private void sortTasks(@NotNull final List<TaskDTO> taskList, @NotNull final SessionDTO sessionDTO) {
        if (serviceLocator != null) {
            System.out.println("PLEASE CHOOSE TYPE OF SORT OF TASKS: ");
            System.out.println("1. DATE OF CREATE;");
            System.out.println("2. DATE OF BEGIN;");
            System.out.println("3. DATE OF END;");
            System.out.println("4. STATUS.");
            System.out.println("ENTER NAME OF SORT OR NO");
            @NotNull final String typeOfSort = serviceLocator.getTerminalService().nextLine();
            switch (typeOfSort) {
                case "1":
                case "DATE OF CREATE":
                    Collections.sort(taskList, new Comparator<TaskDTO>() {
                        @Override
                        public int compare(TaskDTO t1, TaskDTO t2) {
                            return t1.getDateOfCreate().compare(t2.getDateOfCreate());
                        }
                    });
                    break;
                case "2":
                case "DATE OF BEGIN":
                    Collections.sort(taskList, new Comparator<TaskDTO>() {
                        @Override
                        public int compare(TaskDTO t1, TaskDTO t2) {
                            return t1.getDateOfBegin().compare(t2.getDateOfBegin());
                        }
                    });
                    break;
                case "3":
                case "DATE OF END":
                    Collections.sort(taskList, new Comparator<TaskDTO>() {
                        @Override
                        public int compare(TaskDTO t1, TaskDTO t2) {
                            return t1.getDateOfEnd().compare(t2.getDateOfEnd());
                        }
                    });
                    break;
                case "4":
                case "STATUS":
                    Collections.sort(taskList, new Comparator<TaskDTO>() {
                        @Override
                        public int compare(TaskDTO t1, TaskDTO t2) {
                            return t1.getStatus().compareTo(t2.getStatus());
                        }
                    });
                    break;
                case "NO":
                    break;
            }
            int index = 1;
            if (taskList.isEmpty()) System.out.println("TASK LIST IS EMPTY");
            for (@Nullable final TaskDTO task : taskList) {
                if (task == null) return;
                System.out.println(index++ + ". " + task.getName());
            }
            System.out.println();
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
