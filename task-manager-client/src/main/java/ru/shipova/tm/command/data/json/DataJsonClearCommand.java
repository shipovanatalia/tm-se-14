package ru.shipova.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.DataConstant;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.Session;
import ru.shipova.tm.endpoint.SessionDTO;

import java.io.File;
import java.io.PrintWriter;

public class DataJsonClearCommand extends AbstractCommand {
    @Override
    public @Nullable String getName() {
        return "json-clear";
    }

    @Override
    public @Nullable String getDescription() {
        return "Clear JSON-file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                return;
            }
            if (!adminUserEndpoint.haveAccessToAdminCommand(sessionDTO, isOnlyAdminCommand())) {
                System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                return;
            }
            System.out.println("[DATA JSON CLEAR]");
            @NotNull final File file = new File(DataConstant.FILE_JSON.displayName());
            @NotNull final PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.flush();
            writer.close();
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
