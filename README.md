**SOFTWARE**
- Windows 10<br/>
- JRE 1.8<br/>
<br/>

**TECHNOLOGIES**
- Java JDK 1.8<br/>
- Maven 3<br/>
<br/>

**BUILD APPLICATION WITH MAVEN**<br/>
```
mvn clean install
```
<br/>

**RUN APPLICATION**<br/>
```
java -jar task-manager-1.0-SNAPSHOT.jar
```
<br/>

**DEVELOPER**<br/>
Shipova Natalia <nataliefrance@mail.ru>
