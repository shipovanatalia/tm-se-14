package ru.shipova.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@NoArgsConstructor
public final class UserRepository {

    @Nullable
    private EntityManager entityManager;

    public UserRepository(@Nullable EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull final User user){
        if (entityManager == null) return;
        entityManager.persist(user);
    }

    public void merge(@NotNull final User user){
        if (entityManager == null) return;
        entityManager.merge(user);
    }

    @Nullable
    public User findById(@NotNull final String id){
        if (entityManager == null) return null;
        return entityManager.find(User.class, id);
    }

    @Nullable
    public List<User> findAll(){
        if (entityManager == null) return null;
        return entityManager.createQuery("select u from User u").getResultList();
    }

    @Nullable
    public User findByLogin(@NotNull final String userLogin){
        if (entityManager == null) return null;
        @NotNull final String query = "Select u from User u where u.login = :userLogin";
        @NotNull final User user;
        try {
            user = entityManager.createQuery(query, User.class)
                    .setParameter("userLogin", userLogin)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return user;
    }
}
