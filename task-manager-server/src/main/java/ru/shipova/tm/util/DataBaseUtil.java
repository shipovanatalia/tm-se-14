package ru.shipova.tm.util;

import lombok.NoArgsConstructor;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.SessionRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.repository.UserRepository;

import javax.annotation.Nullable;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@NoArgsConstructor
public final class DataBaseUtil {
    public static EntityManagerFactory getFactory() {
        final InputStream inputStream = DataBaseUtil.class.getClassLoader().getResourceAsStream("db.properties");
        final Properties property = new Properties();
        try {
            property.load(inputStream);
        } catch (IOException e) {
            System.out.println("ERROR WHILE LOADING DATA BASE PROPERTIES FILE");
            e.printStackTrace();
        }
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, property.getProperty("jdbc.driver"));
        settings.put(Environment.URL, property.getProperty("db.host"));
        settings.put(Environment.USER, property.getProperty("db.login"));
        settings.put(Environment.PASS, property.getProperty("db.password"));
        settings.put(Environment.DIALECT,
                "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}

