package ru.shipova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.AccessForbiddenException;

public interface ISessionService {
    @Nullable
    SessionDTO open(@NotNull User user);

    @Nullable Result close(@NotNull final Session session);

    void validate(@Nullable final Session session) throws AccessForbiddenException;
}
