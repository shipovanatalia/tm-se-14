package ru.shipova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.dto.DomainDTO;
import ru.shipova.tm.entity.Session;

import java.io.IOException;

public interface IDomainService {
    void load(@Nullable final DomainDTO domain);
    void export(@NotNull final DomainDTO domain);
    void serialize(@NotNull final Session session, @NotNull final DomainDTO domain, @NotNull final String serializer) throws IOException;
    void deserialize(@NotNull final Session session, @NotNull final String serializer);
}
