package ru.shipova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;

import java.util.List;

public interface IUserService {

    void createUser(@Nullable final String userLogin, @Nullable final String userPass, @Nullable final RoleType roleType) throws LoginAlreadyExistsException;

    User authorize(@NotNull final String login, @NotNull final String password);

    void registryUser(@Nullable final String login, @Nullable final String password, @Nullable final String role) throws LoginAlreadyExistsException;

    void updateUser(@Nullable final String login, @Nullable final String role);

    RoleType getRoleType(@NotNull final Session session);

    RoleType getRoleType(@Nullable final String role);

    void setNewPassword(@Nullable final String userLogin, @NotNull final String password);

    User findById(@NotNull final String userId);

    @Nullable List<User> getUserList();

    void load(@Nullable final List<User> userList);
}
