package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.dto.DomainDTO;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.util.EntityConvertUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;

@WebService(name = "DomainEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class DomainEndpoint extends AbstractEndpoint {
    public DomainEndpoint() {
        super(null);
    }

    public DomainEndpoint(@Nullable IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

//    @WebMethod
//    public void load(
//            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
//            @WebParam(name = "domain", partName = "domain") @Nullable final Domain domain
//    ) throws AccessForbiddenException {
//        if (serviceLocator == null) return;
//        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
//        serviceLocator.getISessionService().validate(session);
//        serviceLocator.getIDomainService().load(domain);
//    }

    @WebMethod
    public void serialize(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "serializer", partName = "serializer") @NotNull final String serializer
    ) throws AccessForbiddenException, IOException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        domainDTO.setUserId(session.getUser().getId());
        serviceLocator.getIDomainService().export(domainDTO);
        serviceLocator.getIDomainService().serialize(session, domainDTO, serializer);
    }

    @WebMethod
    public void deserialize(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "serializer", partName = "serializer") @NotNull final String deserializer
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIDomainService().deserialize(session, deserializer);
    }
}
