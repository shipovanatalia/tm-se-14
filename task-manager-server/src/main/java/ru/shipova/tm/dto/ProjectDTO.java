package ru.shipova.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ProjectDTO extends AbstractFieldOfUserDTO implements Serializable {

    public static final long serialVersionUID = 1;

    @Nullable
    private String name;

    @Nullable
    private String userId;

    @NotNull
    private String description = "";

    public ProjectDTO(@NotNull final String id, @Nullable final String userId, @Nullable final String name) {
        this.id = id;
        this.name = name;
        this.userId = userId;
    }
}
