package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;

import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public final class TaskService implements ITaskService {
    private @Nullable
    TaskRepository taskRepository;
    private @Nullable
    ProjectRepository projectRepository;

    private @NotNull EntityManagerFactory entityManagerFactory;

    public TaskService(@NotNull final IServiceLocator serviceLocator) {
        this.entityManagerFactory = serviceLocator.getEntityManagerFactory();
    }

    @NotNull
    @Override
    public List<Task> showAllTasksOfProject(@NotNull final String projectName) throws ProjectDoesNotExistException {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        projectRepository = new ProjectRepository(entityManager);
        taskRepository = new TaskRepository(entityManager);
        if (projectName.isEmpty()) throw new ProjectDoesNotExistException();
        @Nullable final Project project = projectRepository.getProjectByName(projectName);
        if (project == null) throw new ProjectDoesNotExistException();
        @NotNull final String projectId = project.getId();
        @NotNull List<Task> taskList = taskRepository.showAllTasksOfProject(projectId);
        entityManager.close();
        return taskList;
    }

    @Nullable
    @Override
    public List<Task> getTaskListOfUser(@Nullable final String userId) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        taskRepository = new TaskRepository(entityManager);
        if (userId == null || userId.isEmpty()) return null;
        @NotNull List<Task> taskList = taskRepository.findAllByUserId(userId);
        entityManager.close();
        return taskList;
    }

    @Override
    public void load(@Nullable final List<Task> taskList) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        taskRepository = new TaskRepository(entityManager);
        if (taskList == null) return;
        try {
            entityManager.getTransaction().begin();
            for (@Nullable final Task task : taskList) {
                if (task == null) return;
                taskRepository.persist(task);
            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Task> search(@Nullable final String userId, @Nullable final String partOfData) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        taskRepository = new TaskRepository(entityManager);
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        @Nullable List<Task> taskList = taskRepository.search(userId, partOfData);
        entityManager.close();
        return taskList;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String taskName, @Nullable final String projectName) throws ProjectDoesNotExistException {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        taskRepository = new TaskRepository(entityManager);
        projectRepository = new ProjectRepository(entityManager);
        if (userId == null || userId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        if (projectName == null || projectName.isEmpty()) return;
        @Nullable final Project project = projectRepository.getProjectByName(projectName);
        if (project == null) throw new ProjectDoesNotExistException();
        @NotNull final String projectId = project.getId();
        try {
            entityManager.getTransaction().begin();
            @NotNull final String taskId = UUID.randomUUID().toString();
            @NotNull final Task task = new Task();
            task.setId(taskId);
            task.setName(taskName);
            task.setProject(project);
            task.setUser(project.getUser());
            taskRepository.persist(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        taskRepository = new TaskRepository(entityManager);
        if (userId == null || userId.isEmpty()) return;
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskName) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        taskRepository = new TaskRepository(entityManager);
        if (taskName == null || taskName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @Nullable final Task task = taskRepository.getTaskByName(taskName);
        if (task == null) return;
        @NotNull final String taskId = task.getId();
        try {
            entityManager.getTransaction().begin();
            task.setDateOfEnd(new Date());
            if (!userId.equals(task.getUser().getId())) return;
            taskRepository.removeByTaskId(taskId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }
}
