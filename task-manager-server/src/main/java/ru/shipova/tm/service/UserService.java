package ru.shipova.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;
import ru.shipova.tm.repository.UserRepository;
import ru.shipova.tm.util.DataBaseUtil;
import ru.shipova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public final class UserService implements IUserService {

    private @NotNull UserRepository userRepository;
    private @NotNull EntityManagerFactory entityManagerFactory;

    public UserService(@NotNull final IServiceLocator serviceLocator) {
        this.entityManagerFactory = serviceLocator.getEntityManagerFactory();
    }

    @Override
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (roleType == null) return;
        @NotNull final String userId = UUID.randomUUID().toString();
        @Nullable final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null) return;
        @NotNull final User user = new User(userId, login, passwordHash, roleType);
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.persist(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void registryUser(@Nullable final String login, @Nullable final String password, @Nullable final String role) throws LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (role == null || role.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        userRepository = new UserRepository(entityManager);
        if (userRepository.findByLogin(login) != null) throw new LoginAlreadyExistsException();
        @Nullable final String passwordHash = HashUtil.md5(password);
        @NotNull final String userId = UUID.randomUUID().toString();
        @Nullable final RoleType roleType = getRoleType(role);
        @NotNull final User user = new User(userId, login, passwordHash, roleType);
        try {
            entityManager.getTransaction().begin();
            userRepository.persist(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User authorize(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) return null;
        if (password.isEmpty()) return null;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        userRepository = new UserRepository(entityManager);
        @Nullable User user = userRepository.findByLogin(login);
        entityManager.close();
        if (user == null) return null;
        @Nullable final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null) return null;
        if (passwordHash.equals(user.getPasswordHash())) {
            return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findById(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        userRepository = new UserRepository(entityManager);
        @Nullable final User user = userRepository.findById(userId);
        entityManager.close();
        return user;
    }

    @Nullable
    @Override
    public RoleType getRoleType(@NotNull final Session session) {
        @Nullable final User user = findById(session.getUser().getId());
        if (user == null) return null;

        @Nullable final RoleType roleType = user.getRoleType();
        return roleType;
    }

    @Nullable
    @Override
    public RoleType getRoleType(@Nullable final String role) {
        if (role == null || role.isEmpty()) return null;
        RoleType roleType = null;
        switch (role.toUpperCase()) {
            case "USER":
                roleType = RoleType.USER;
                break;
            case "ADMIN":
                roleType = RoleType.ADMIN;
                break;
        }
        return roleType;
    }

    @Override
    public void updateUser(@Nullable final String login, @Nullable final String role) {
        if (role == null || role.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        userRepository = new UserRepository(entityManager);
        @Nullable final RoleType roleType = getRoleType(role);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setRoleType(roleType);
        try {
            entityManager.getTransaction().begin();
            user.setLogin(login);
            user.setRoleType(roleType);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void setNewPassword(@Nullable final String login, @NotNull final String password) {
        if (password.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        userRepository = new UserRepository(entityManager);
        @Nullable final String passwordHash = HashUtil.md5(password);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setPasswordHash(passwordHash);
        try {
            entityManager.getTransaction().begin();
            user.setPasswordHash(passwordHash);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<User> getUserList() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        userRepository = new UserRepository(entityManager);
        @Nullable final List<User> userList = userRepository.findAll();
        entityManager.close();
        return userList;
    }

    @Override
    public void load(@Nullable final List<User> userList) {
        if (userList == null) return;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@Nullable final User user : userList) {
                if (user == null) return;
                userRepository.merge(user);
            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }
}