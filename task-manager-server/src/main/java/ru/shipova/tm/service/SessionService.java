package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.service.ISessionService;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.repository.SessionRepository;
import ru.shipova.tm.util.EntityConvertUtil;
import ru.shipova.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Date;
import java.util.UUID;

public class SessionService implements ISessionService {

    private @Nullable SessionRepository sessionRepository;
    private @NotNull EntityManagerFactory entityManagerFactory;
    private @Nullable IServiceLocator serviceLocator;

    @NotNull
    public SessionService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.entityManagerFactory = serviceLocator.getEntityManagerFactory();
    }

    @Override
    @Nullable
    public SessionDTO open(@NotNull User user) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        sessionRepository = new SessionRepository(entityManager);
        @Nullable SessionDTO sessionDTO = null;
        try {
            entityManager.getTransaction().begin();
            if (serviceLocator == null) return null;
            sessionDTO = new SessionDTO();
            sessionDTO.setId(UUID.randomUUID().toString());
            sessionDTO.setUserId(user.getId());
            sessionDTO.setTimestamp(new Date().getTime());
            @NotNull final String salt = "SomeVerySpicySalt";
            @Nullable final String signature = SignatureUtil.sign(sessionDTO, salt, 100);
            if (signature == null || signature.isEmpty()) return null;
            sessionDTO.setSignature(signature);
            @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
            sessionRepository.persist(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return sessionDTO;
    }

    @Override
    @Nullable
    public Result close(@NotNull final Session session) {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeBySessionId(session.getId());
            entityManager.getTransaction().commit();
            return new Result();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessForbiddenException {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        sessionRepository = new SessionRepository(entityManager);
        if (session == null) throw new AccessForbiddenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if (session.getTimestamp() == null) throw new AccessForbiddenException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        SignatureUtil.sign(temp, "SomeVerySpicySalt", 100);
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
        if (sessionRepository.findBySessionId(session.getId()) == null) throw new AccessForbiddenException();
        entityManager.close();
    }
}
