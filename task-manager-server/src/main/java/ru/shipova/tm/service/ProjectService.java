package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.TaskRepository;

import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.UUID;

public final class ProjectService implements IProjectService {
    private @Nullable
    TaskRepository taskRepository;
    private @Nullable
    ProjectRepository projectRepository;

    private @NotNull EntityManagerFactory entityManagerFactory;

    public ProjectService(@NotNull final IServiceLocator serviceLocator) {
        this.entityManagerFactory = serviceLocator.getEntityManagerFactory();
    }

    @Override
    @Nullable
    public List<Project> getProjectListOfUser(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        projectRepository = new ProjectRepository(entityManager);
        @Nullable final List<Project> projectList = projectRepository.getProjectListByUserId(userId);
        entityManager.close();
        return projectList;
    }

    @Override
    public void load(@Nullable final List<Project> projectList) {
        if (projectList == null) return;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@Nullable final Project project : projectList) {
                if (project == null) return;
                projectRepository.merge(project);
            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<Project> search(@Nullable final String userId, @Nullable final String partOfData) {
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        projectRepository = new ProjectRepository(entityManager);
        @Nullable final List<Project> projectList = projectRepository.search(userId, partOfData);
        entityManager.close();
        return projectList;
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String projectName) {
        @NotNull final String projectId = UUID.randomUUID().toString();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Project project = new Project();
            project.setName(projectName);
            @NotNull final User user = new User();
            user.setId(userId);
            project.setUser(user);
            projectRepository.persist(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        projectRepository = new ProjectRepository(entityManager);
        taskRepository = new TaskRepository(entityManager);
        @Nullable final List<Project> projectList = projectRepository.getProjectListByUserId(userId);
        if (projectList == null) return;
        try {
            entityManager.getTransaction().begin();
            for (@Nullable final Project project : projectList) {
                if (project == null) return;
                taskRepository.removeAllTasksOfProject(project.getId());
            }
            projectRepository.removeAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        projectRepository = new ProjectRepository(entityManager);
        taskRepository = new TaskRepository(entityManager);
        @Nullable final List<Project> projectList = projectRepository.getProjectListByUserId(userId);
        if (projectList == null) return;
        try {
            entityManager.getTransaction().begin();
            for (@Nullable final Project project : projectList) {
                if (project == null) return;
                @NotNull final String projectId = project.getId();
                if (project.getName().equals(projectName))
                    projectRepository.removeByProjectId(projectId);
            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }
}
