package ru.shipova.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.endpoint.ISessionEndpoint;
import ru.shipova.tm.api.service.*;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.endpoint.*;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;
import ru.shipova.tm.service.*;
import ru.shipova.tm.util.DataBaseUtil;

import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.List;

/**
 * Класс загрузчика приложения
 */

public final class Bootstrap implements IServiceLocator {
    @NotNull
    private final EntityManagerFactory entityManagerFactory = DataBaseUtil.getFactory();
    @Getter
    @NotNull
    private final PropertyService propertyService = new PropertyService();
    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);
    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);
    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);
    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);
    @Getter
    @NotNull
    private final IAdminUserService adminUserService = new AdminUserService(userService);
    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @Getter
    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @Getter
    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);
    @Getter
    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpoint(this);
    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    public void init() {
        initEndpoint();
        @Nullable final List<User> userList;
        userList = userService.getUserList();
        if (userList == null || userList.isEmpty()) initDefaultUsers();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(projectEndpoint);
        registry(domainEndpoint);
        registry(userEndpoint);
        registry(taskEndpoint);
        registry(adminUserEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull Integer port = propertyService.getServerPort();
        @NotNull String name = endpoint.getClass().getSimpleName();
        @NotNull String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(wsdl, endpoint);
    }

    private void initDefaultUsers() {
        try {
            userService.createUser("user", "user", RoleType.USER);
            userService.createUser("admin", "admin", RoleType.ADMIN);
        } catch (LoginAlreadyExistsException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public IProjectService getIProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getITaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getIUserService() {
        return userService;
    }

    @Override
    public @NotNull IDomainService getIDomainService() {
        return domainService;
    }

    @Override
    public @NotNull ISessionService getISessionService() {
        return sessionService;
    }

    @Override
    public @NotNull IAdminUserService getIAdminUserService() {
        return adminUserService;
    }

    @Override
    public @NotNull EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }
}
